import React from "react";   
import { NavLink } from "react-router-dom";
import homeImg from "../assets/images/home.svg";
import 'aos';
import "./home.css";


const Home = () => {

  return (
    <div className="container">
      <div className="row">
        <div data-aos="fade-right" className="primary-col col-md-6">
          <img  className="homeImg" src={homeImg} />
        </div>
        <div className="secondary-col col-md-6">
          <h2 className="text-center text-primary">Let's Fetch </h2>
          <h3 className="text-center text-success"><strong>Data</strong></h3>
          <p>
            Data is essentially the <strong>plain facts and statistics</strong> collected during
            the operations of a business. They can be used to measure/record a
            wide range of business activities - both internal and external.
          </p>
          <NavLink to="/table"><button className="btn btn-dark">Let's Start</button></NavLink>
        </div>
      </div>
    </div>
  );
};
export default Home;
