import React, {useState, useEffect} from "react";
import axios from "axios";

const Table = () => {
  const [data, setData] = useState([]);
  const [search, setSearch] = useState();
  const [viewData, setViewData] = useState(3);

// const searchData = (rows) => {
//   rows.filter(row =>row.author.toLowerCase().indexOf(search)> -1)
// }

  const fetchData = () =>{
    const url =
      `https://newsapi.org/v2/everything?q=${search}&apiKey=20c3c7f7bac14068a579965a8bcd6fef`;
      axios.get(url).then((response) => {
      setData(response?.data);
    });
  }

  useEffect(() => fetchData(), []);
 

  return (
    <div className="container mt-2">
      <div className="row">
        <div className="col-md-6"></div>
        <div className="col-md-6">
          <div className="d-flex">
            <input
              className="form-control me-2"
              // value={search}
              onChange={(event)=> setSearch(event.target.value)}
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              onClick={fetchData}
              className="btn btn-success"
              type="submit"
            >
              Search
            </button>
          </div>
        </div>
      </div>
      <table  className="table table-responsive table-striped">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Author</th>
            <th scope="col">Title</th>
            <th scope="col">Description</th>
            <th scope="col">Images</th>
          </tr>
        </thead>
        <tbody>
          {data?.articles?.slice(0,viewData).map((element, index) => (
            <tr>
              <td>{index + 1}</td>
              <td>
                {element.author ? (
                  element.author
                ) : (
                  <p className="text-danger">Anonymous</p>
                )}
              </td>
              <td>{element.title}</td>
              <td>
                {element.description}
                <a
                  style={{ textDecoration: "none" }}
                  className="text-primary"
                  href={element.url}
                >
                  Read More
                </a>
              </td>
              <td><img className="img img-thumbnail img-responsive" src = {element.urlToImage} alt="No Image to Display"/></td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="text-center">
  
        <button onClick={()=>setViewData(viewData + 3)} className="btn btn-info">
          View More
        </button>
    
      </div>
    </div>
  );
};

export default Table;
