import React from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import Navbar from "./components/navbar";
import Home from "./components/home";
import Table from "./components/table";
import "./App.css";

function App() {
  return (
    <div>
      <Router>
        <Navbar />
        <Routes>
          <Route path="/home" element={<Home />} />
          <Route path="/table" element={<Table />} />
          <Route path="*" element={<><h2 className="text-danger">Page Not Found!</h2></>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
